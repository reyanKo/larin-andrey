package Lesson5;

import java.util.Random;
import java.util.Scanner;

public class Zadacha2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Добро пожаловать в масив с твоми размерами: ");
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int[][] data = new int[a][b];
        System.out.println("Зарандомь свой масив: ");
        int c = scanner.nextInt();
        randommas(data, c);
        printmas(data);
        sortmas(data, a, b);
        System.out.println("Таки после сортировки по убыванию родилось вот такое: ");
        sortmas(data, a, b);
        printmas(data);

    }

    static void sortmas(int[][] array, int a, int b) {
        int bom = 0;
        for (int i = 0; i < a * b; i++) {
            for (int j = 0; j < array.length; j++) {
                for (int k = 0; k < array[j].length - 1; k++) {
                    if (array[j][k] < array[j][k+1]) {
                        bom = array[j][k];
                        array[j][k] = array[j][k + 1];
                        array[j][k + 1] = bom;
                    }

                }
                if (j != a - 1 && array[j][b - 1] < array[j + 1][0]) {
                    int bam = array[j][b - 1];
                    array[j][b - 1] = array[j + 1][0];
                    array[j + 1][0] = bam;
                }

            }

        }
    }

    static void randommas(int[][] array, int a) {
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                array[i][j] = random.nextInt(a);

            }

        }
    }

    static void printmas(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                System.out.print(array[i][j] + " ");

            }
            System.out.println();

        }
    }
}
