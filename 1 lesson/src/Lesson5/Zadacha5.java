package Lesson5;

import java.util.Random;
import java.util.Scanner;

public class Zadacha5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите масив: ");
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int[][] data = new int[a][b];
        System.out.println("Введите наш рандом: ");
        int c = scanner.nextInt();
        randmass(data, c);
        printmass(data);
        System.out.println();
        sortstroke(data);
        printmass(data);


    }

    static void sortstroke(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length -1; j++) {
                if (sumstrokechet(array[j]) > sumstrokechet(array[j + 1])) {
                    int[] bam = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = bam;


                }


            }
        }
    }

    static int sumstrokechet(int[] array) {
        int bam = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 == 0) {
                bam += array[i];
            }
        }
        return bam;
    }

    static void randmass(int[][] array, int a) {
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = random.nextInt(a);

            }
        }
    }

    static void printmass(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + " ");

            }
            System.out.print("= " + sumstrokechet(array[i]) );
            System.out.println();

        }
    }
}
