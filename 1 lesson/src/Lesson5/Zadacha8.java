package Lesson5;

import java.util.Random;
import java.util.Scanner;

public class Zadacha8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Задайте масив который вам интересен ^_^ : ");
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int[][] data = new int[a][b];
        System.out.println("Введи максимум рандома который тебе интересен: ");
        int c = scanner.nextInt();
        random(data, c);
        System.out.println("Вот ваш масив: ");
        print(data);
        System.out.println("Столбцы с числом правды: " + poiskchis(data, b));

    }

    static void changeColuma(int[][] array, int a, int b) { //Обмен столбцов которым мы сами выдаём переменную столбца.
        int bam = 0;
        for (int i = 0; i < array.length; i++) {
            bam = array[i][a];
            array[i][a] = array[i][b];
            array[i][b] = bam;
        }
    }

    static int poiskchis(int[][] array, int a) {
        int bam = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                if (prosto(array[i])) {
                    bam++;

                }
            }
        }
        return bam;
    }

    static boolean prosto(int[] array) {
        boolean bam = false;
        for (int i = 0; i < array.length; i++) {
            for (int j = 2; j <= array[i]; j++) {
                if (j == array[i]) {
                    bam = true;
                    continue;
                } else if (array[i] % j == 0) {
                    j = array[i];

                }
            }
        }
        return bam;
    }

    static void random(int[][] array, int a) {
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = random.nextInt(a);
            }
        }
    }

    static void print(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
    }
}
