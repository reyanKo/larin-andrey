package Lesson5;

import java.util.Random;

public class Zadacha0 {
    public static void main(String[] args) {
        int[][] data = new int[5][7];
        fillrandom(data, 50);//Задаёт случайные числа в масиве

        System.out.println("Before");
        ;

        printarray(data); //Печать масива

        sort2xArray(data);//Сортировка масива по сумме

        System.out.println("After");

        printarray(data); //Печать масива
    }

    static void sort2xArray(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length - 1; j++) {
                if (sumElemArray(array[j]) < sumElemArray(array[j + 1])) {
                    //меняем местами строки в масиве
                    int[] tmp;
                    tmp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = tmp;
                }

            }

        }

    } //Сортировка Масива по сумме в строках.

    static int sumElemArray(int[] data) {
        int res = 0;
        for (int i = 0; i < data.length; i++) {
            res += data[i];

        }
        return res;
    }//Сумма элементов в строке.

    static void printarray(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                System.out.print(array[i][j] + " ");

            }
            System.out.println(" = " + sumElemArray(array[i]));
            System.out.println();
        }
    }//Печать масива.

    static void fillrandom(int[][] array, int bound) {
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                array[i][j] = random.nextInt(bound) + 10;

            }

        }
    }//Задача рандомных чисел в масиве.
}
