package Lesson5;

import java.util.Random;
import java.util.Scanner;

public class Zadacha3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите размерность масива: ");
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int[][] data = new int[a][b];
        System.out.println("А теперь придумай числа в масиве: ");
        int c = scanner.nextInt();
        randmas(data,c);
        printmas(data);
        System.out.println();
        changemas(data);
        sortell(data);
        changemas(data);
        System.out.println();
        printmas(data);

    }
   static void sortell(int[][]array){
       int bam= 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length-1; j++) {
                if (summelst(array[j])>summelst(array[j+1]) ){

                    bam = array [i][j];
                    array[i][j] = array[i][j+1];
                    array[i][j+1] = bam;
                }

            }
        }
    }

   static void changemas(int[][]array){
       int bam = 0;
       for (int i = 0; i < array.length; i++) {
           for (int j = i+1; j < array.length; j++) {
               bam = array[i][j];
               array[i][j] = array[j][i];
               array[j][i] = bam;
           }
       }
   }

    static int summelst(int[]array){
        int bam = 0;
        for (int i = 0; i <array.length ; i++) {
             bam += array[i];
        }
        return bam;
    }

    static void printmas(int[][]array){
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + " ");
                }
            System.out.println();
        }
    }

    static void randmas(int[][]array,int a){
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = random.nextInt(a);
            }
        }
    }
}
