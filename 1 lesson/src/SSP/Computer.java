package SSP;

import java.util.Random;

public class Computer implements Player1 {
    int point;
    String name;

    {
        point = 0;
        name = "computer";
    }

    @Override
    public String getName() {
        Random random = new Random();
        int a = random.nextInt(16);
        int b = random.nextInt(19);
        String[] names = {"Василий", "Человек", "Медведь", "Почтальон", "Мальчик", "Андрей", "Теоретик", "Практик", "Творец",
                "Иудей", "Вандам", "Траволта", "Брадобрей", "Черновик", "Соловей", "Карандаш", "Крот"};
        String[] snames = {"Большой", "Маленький", "Смешной", "Корявый", "Испорченный", "Татальный", "Прилипчивый", "Весёлый",
                "Странный", "Улыбчивый", "Каварный", "Худший", "Лучший", "Тёплый", "Мягкий", "Сутулый", "Кошерный", "Умный", "Тупой", "Кислотный"};
        name = snames[a] + " " + names[b];


        return name;
    }

    @Override
    public int getMove() {
        Random random = new Random();
        return random.nextInt(3) + 1;
    }

    @Override
    public void addPoint() {
        point++;
    }

    @Override
    public void getPoints(int b, int c) {
        if (c == 1 && b == 2) {
            addPoint();
            System.out.println("Computer WIN");

        }
        if (c == 3 && b == 1) {
            addPoint();
            System.out.println("Computer WIN");

        }
        if (c == 2 && b == 3) {
            addPoint();
            System.out.println("Computer WIN");

        }
    }
}
