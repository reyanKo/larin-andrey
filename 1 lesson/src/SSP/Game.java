package SSP;

import java.util.Scanner;

public class Game {
    public static void play() {
        Human human = new Human();
        Computer computer = new Computer();
        System.out.println("The game will begun!!!");
        System.out.println("----------------------");
        System.out.println(computer.getName() + " vs " + human.getName());
        Game.game(computer, human);
    }

    public static void game(Computer computer, Human human) {
        int a = human.getMove();
        int b = computer.getMove();
        computer.getPoints(a, b);
        human.getPoints(a, b);
        System.out.println(computer.name + " " + computer.point + " vs " + human.name +" " + human.point);
        System.out.println("----------------------");
        System.out.println("Try again? y/n");
        Scanner scanner = new Scanner(System.in);
        String c = scanner.nextLine();
        if (c.equals("y")) {
            Game.game(computer, human);
        }
    }
}
