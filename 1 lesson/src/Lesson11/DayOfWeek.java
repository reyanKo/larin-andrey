package Lesson11;

public enum DayOfWeek {
    MONDAY(8),
    TUESDAY(8),
    WEDNESDAY(8),
    THURSDAY(7),
    FRIDAY(0),
    SATURDAY(0),
    SUNDAY(8);
    private int work;

    DayOfWeek(int work) {
        this.work = work;
    }

    public int Ours() {
        return this.work;
    }
}
