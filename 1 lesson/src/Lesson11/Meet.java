package Lesson11;

import java.util.Scanner;

public class Meet {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Сколько у вас сегодня встреч? ");
        int a = scanner.nextInt();
        DateOfMeet[] meet = new DateOfMeet[a];
        for (int i = 0; i < meet.length; i++) {
            meet[i] = new DateOfMeet();
        }
        zadacha(meet);
    }

    public static void zadacha(DateOfMeet[] meet) {
        DayOfWeek[] dayOfWeeks = DayOfWeek.values();
        Month[] months = Month.values();
        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < meet.length; i++) {
            System.out.println("Какой день встречи?");
            System.out.println("0-п;1-в;2-с;3-ч;4-п;5-c;6-в");
            int b = scanner.nextInt();
            meet[i].day = dayOfWeeks[b];
            System.out.println("Какой месяц встречи? 0-11 Соответственно");
            int c = scanner.nextInt();
            meet[i].month = months[c];
            System.out.println("В какой час?");
            meet[i].ours = scanner.nextInt();
            System.out.println("Во сколько минут?");
            meet[i].minuts = scanner.nextInt();
            System.out.println("Какое название встречи?");
            meet[i].name = scanner.next();
            System.out.println("Какие детали встречи?");
            meet[i].details = scanner.next();
        }
    }
}
