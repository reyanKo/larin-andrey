package Lesson11;

public class main {
    public static void main(String[] args) {
        System.out.println(getWorkingHours(DayOfWeek.FRIDAY));
    }

    public static int getWorkingHours(DayOfWeek dayOfWeek) {
        int a = 0;
        DayOfWeek[] day = DayOfWeek.values();
        switch (dayOfWeek) {
            case MONDAY:
                for (int i = 0; i < day.length; i++) {
                    a += day[i].Ours();
                    break;
                }
            case TUESDAY:
                for (int i = 1; i < day.length; i++) {
                    a += day[i].Ours();
                    break;
                }
            case WEDNESDAY:
                for (int i = 2; i < day.length; i++) {
                    a += day[i].Ours();
                    break;
                }
            case THURSDAY:
                for (int i = 3; i < day.length; i++) {
                    a += day[i].Ours();
                    break;
                }
            case FRIDAY:
                for (int i = 4; i < day.length; i++) {
                    a += day[i].Ours();
                    break;
                }
            case SATURDAY:
                for (int i = 5; i < day.length; i++) {
                    a += day[i].Ours();
                    break;
                }
            case SUNDAY:
                for (int i = 6; i < day.length; i++) {
                    a += day[i].Ours();
                    break;
                }
        }
        return a;
    }
}
