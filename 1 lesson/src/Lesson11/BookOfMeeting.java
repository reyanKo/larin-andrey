package Lesson11;

import java.util.Scanner;

public class BookOfMeeting {
    public static void main(String[] args) {
        DateOfMeet[] days = new DateOfMeet[365];
        array(days);
        addMeet(days);
        print(days);
    }

    public static void addMeet(DateOfMeet[] array) {
        Scanner scanner = new Scanner(System.in);
        DateOfMeet date = new DateOfMeet();
        System.out.println("В какой месяц вы ходите назначить встречу? (0-11)");
        Month[] months = Month.values();
        int a = scanner.nextInt();
        date.month = months[a];
        System.out.println("На какой день вы хотите назначить встречу? ");
        date.monday = scanner.nextInt();
        System.out.println("Во сколько вы хотите назачить встречу? ");
        date.ours = scanner.nextInt();
        for (int i = 0; i < array.length; i++) {
            if (date.month == array[i].month && date.monday == array[i].monday && array[i].ours == -1) {
                date.day = array[i].day;
                array[i] = date;
                System.out.println("Какое название вашей встречи: ");
                array[i].name = scanner.next();
                System.out.println("Какие детали вашей встречи? ");
                array[i].details = scanner.next();
                System.out.println("Во сколько минут будет встреча?");
                array[i].minuts = scanner.nextInt();
            }
        }
    }

    public static void array(DateOfMeet[] days) {
        DayOfWeek[] day = DayOfWeek.values();
        int a = 0;
        int b = 0;
        for (int i = 0; i < days.length; i++) {
            days[i] = new DateOfMeet();
            a++;
            b++;
            days[i].monday = b;
            days[i].day = day[a];
            if (a == 6) {
                a = 0;
            }
            if (i >= 0 && i <= 30) {
                if (b == 31) {
                    b = 0;
                }
                days[i].month = Month.JANUARY;
            }
            if (i > 30 && i <= 58) {
                if (b == 28) {
                    b = 0;
                }
                days[i].month = Month.FEBRUARY;
            }
            if (i > 58 && i <= 89) {
                if (b == 31) {
                    b = 0;
                }
                days[i].month = Month.MARCH;
            }
            if (i > 89 && i <= 119) {
                if (b == 30) {
                    b = 0;
                }
                days[i].month = Month.APRIL;
            }
            if (i > 119 && i <= 150) {
                if (b == 31) {
                    b = 0;
                }
                days[i].month = Month.MAY;
            }
            if (i > 150 && i <= 180) {
                if (b == 30) {
                    b = 0;
                }
                days[i].month = Month.JUNE;
            }
            if (i > 180 && i <= 211) {
                if (b == 31) {
                    b = 0;
                }
                days[i].month = Month.JULY;
            }
            if (i > 211 && i <= 242) {
                if (b == 31) {
                    b = 0;
                }
                days[i].month = Month.AUGUST;
            }
            if (i > 242 && i <= 272) {
                if (b == 30) {
                    b = 0;
                }
                days[i].month = Month.SEPTEMBER;
            }
            if (i > 272 && i <= 303) {
                if (b == 31) {
                    b = 0;
                }
                days[i].month = Month.OCTOBER;
            }
            if (i > 303 && i <= 333) {
                if (b == 30) {
                    b = 0;
                }
                days[i].month = Month.NOVEMBER;
            }
            if (i > 333 && i <= 364) {
                if (b == 31) {
                    b = 0;
                }
                days[i].month = Month.DECEMBER;
            }
        }
    }

    public static void print(DateOfMeet[] array) {
        for (int i = 0; i < array.length; i++) {

            if (array[i].ours != -1) {
                System.out.println("Ваша встреча : " + array[i].month + " " + array[i].day + " " + array[i].monday + " " + array[i].name + " " + array[i].details + " " + array[i].ours + " " + array[i].minuts);
            }
        }
    }
}
