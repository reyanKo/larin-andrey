package Lesson11;

public class DateOfMeet {
    DayOfWeek day;
    Month month;
    String name;
    String details;
    int ours;
    int minuts;
    int monday;

    {
        day = DayOfWeek.FRIDAY;
        month = Month.APRIL;
        name = "lolo";
        details = "no";
        ours = 3;
        minuts = 1;
        monday = 0;
    }

    public DateOfMeet() {
        this.day = DayOfWeek.FRIDAY;
        this.month = Month.APRIL;
        this.name = "na";
        this.details = "na";
        this.ours = -1;
        this.minuts = 2;
        this.monday = 0;
    }

    public static void show(DateOfMeet date) {
        System.out.println(date.day + " " + date.month + " " + date.name + " " + date.details + " " + date.ours + " " + date.minuts);
    }
}
