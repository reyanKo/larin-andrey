package Fight;

public abstract class Warior {
    int hp;
    int def;
    int dmg;
    String name;
    String marker;
    boolean undead;

    public Warior(int hp, int def, int dmg, String name, String marker) {
        this.hp = hp;
        this.def = def;
        this.dmg = dmg;
        this.name = name;
        this.marker = marker;
        this.undead = false;
    }

    public void attack(Warior fst, Warior sec) {

    }

    public void deffence(Warior fst) {

    }
}
