package Fight;

import java.util.Random;

public class main {
    //    Создать абстрактный класс воин с полями: здоровье, броня, урон и абстрактными функциями нанести удар, уйти в защиту.
//    Отнаследовать от этого класса класс рыцарь, класс лучник и класс маг.
//    Для каждого реализовать абстрактные функции.
//    Создать два массива, с элементами warrior'а и заполнить каждый из них случайными сочетаниями рыцарей, магов и лучников.
//    После столкнуть два отряда друг с другом. И повторять это до тех пор, пока в отряде будет хоть одна живая единица
    public static void main(String[] args) {
        Random random = new Random();
        int a = random.nextInt(10) + 3;
        Warior[] squ1 = new Warior[a];
        Warior[] squ2 = new Warior[a];
        Logic.squoads(squ1);
        Logic.squoads(squ2);
        Logic.prntsquoad(squ1);
        Logic.prntsquoad(squ2);
        Logic.fight(squ1, squ2);
    }

}
