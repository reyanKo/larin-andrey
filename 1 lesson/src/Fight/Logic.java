package Fight;

import java.util.Random;

public class Logic {


    public static void squoads(Warior[] squoad1) {
        Random random = new Random();
        for (int i = 0; i < squoad1.length; i++) {
            int a = random.nextInt(3) + 1;
            String[] name = {"Вася", "Петя", "Игорь", "Зоя", "Лена", "Лера", "Ира", "Андрей", "Таня", "Даня"};
            int b = random.nextInt(name.length);
            if (a == 1) {
                squoad1[i] = new Knight(100, 7, 15, name[b], "Knight");
            } else if (a == 2) {
                squoad1[i] = new Mage(50, 3, 35, name[b], "Mage");
            } else if (a == 3) {
                squoad1[i] = new Archer(50, 3, 35, name[b], "Archer");
            }
        }
    }

    public static void prntsquoad(Warior[] squ) {
        System.out.println("СКВОД:");
        for (int i = 0; i < squ.length; i++) {
            if (squ[i].undead != true) {
                System.out.println(squ[i].marker + " " + squ[i].name + " " + squ[i].hp);
            }
        }
    }

    public static void fight(Warior[] squ, Warior[] squ1) {
        Random random = new Random();
        for (int i = 0; i < squ.length; i++) {
            int a = random.nextInt(2);
            int b = random.nextInt(2);
            if (b == 0 && squ[i].undead != true && squ1[i].undead != true) {
                squ1[i].attack(squ1[i], squ[i]);
            }
            if (b == 1 && squ1[i].undead != true) {
                squ1[i].deffence(squ1[i]);
            }
            if (a == 0 && squ[i].undead != true && squ1[i].undead != true) {
                squ[i].attack(squ[i], squ1[i]);
            }
            if (a == 1 && squ[i].undead != true) {
                squ[i].deffence(squ[i]);
            }
            if (squ[i].hp <= 0) {
                squ[i].undead = true;
            }
            if (squ1[i].hp <= 0) {
                squ1[i].undead = true;
            }
        }
        for (int i = 0; i < squ.length; i++) {
            for (int j = 1; j < squ.length; j++) {
                if (squ[i].hp < squ[j].hp) {
                    Warior war;
                    war = squ[i];
                    squ[i] = squ[j];
                    squ[j] = war;
                }
            }
        }
        for (int i = 0; i < squ1.length; i++) {
            for (int j = 1; j < squ1.length; j++) {
                if (squ1[i].hp < squ1[j].hp) {
                    Warior war;
                    war = squ1[i];
                    squ1[i] = squ1[j];
                    squ1[j] = war;
                }
            }
        }
        prntsquoad(squ);
        prntsquoad(squ1);
        check(squ, squ1);
    }

    public static void check(Warior[] squ, Warior[] squ1) {
        int a = 0;
        int b = 0;
        for (int i = 0; i < squ.length; i++) {
            if (squ[i].undead == true) {
                a++;
            }
            if (squ1[i].undead == true) {
                b++;
            }
        }
        if (a != squ.length && b != squ1.length) {
            fight(squ, squ1);
        }
    }
}