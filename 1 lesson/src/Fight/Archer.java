package Fight;

public class Archer extends Warior {

    public Archer(int hp, int def, int dmg, String name, String marker) {
        super(hp, def, dmg, name, marker);
    }

    public void attack(Warior fst, Warior sec) {
        System.out.println(fst.name + " атакует "+ sec.name);
        sec.hp = sec.hp + sec.def - fst.dmg;
    }
    public void deffence(Warior fst) {
        System.out.println(fst.name + " Защищается ");
        fst.hp = fst.hp + 1;
    }
}
