package Lesson8;

public class Point {
    private double x;
    private double y;

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public Point() {
        x = 0;
        y = 0;
    }

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public static void print(Point stat) {

        System.out.println("Координаты Х:" + stat.getX());
        System.out.println("Координаты Y:" + stat.getY());
    }

    public static double distanse(Point first, Point second) {
        double set = 0;
        set = Math.sqrt(Math.pow((second.getX() - first.getX()), 2) + Math.pow((second.getY() - first.getY()), 2));
        return set;
    }
}
