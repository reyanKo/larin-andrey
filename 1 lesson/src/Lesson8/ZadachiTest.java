package Lesson8;

public class ZadachiTest {
    public static void main(String[] args) {
        //Задача номер два;
        Fraction first = new Fraction(1, 2);
        Fraction second = new Fraction(3, 7);
        Fraction move = new Fraction();


        System.out.println("Дробь номер один:");
        first.print();
        System.out.println("Дробь номер два:");
        second.print();
        move = FractionLogic.multiply(first, second);
        System.out.println("Умножение дробей даст:");
        move.print();
        move = FractionLogic.division(first, second);
        System.out.println("Деление дробей:");
        move.print();
        move = FractionLogic.plus(first, second);
        System.out.println("Сложение дробей:");
        move.print();
        move = FractionLogic.minus(first, second);
        System.out.println("Вычитание ваших дробей:");
        move.print();

        //Задача номер один;
        Point chekone = new Point(4, 5);
        Point chektwo = new Point(7, -4);

        Point.print(chekone);
        Point.print(chektwo);
        System.out.println("Расстояние от точки до точки: ");
        System.out.println(Point.distanse(chekone, chektwo));

        //Задача номер три;
        Triangle coub = new Triangle(3, 3, 3);
        Triangle.print(coub);
        System.out.println("Периметр нашего треугольника: " + Triangle.perim(coub));
        System.out.println("Площадь по формуле Герона: " + Triangle.geron(coub));
        Triangle.check(coub);

        Human human = new Human();
        Human huuman = new Human("as", "asd", 12, false);
        huuman.prnt();
        human.prnt();

        Figure len = new Figure(10);
        Figure ben = new Figure(10, 10);
        Figure ran = new Figure(10, 10, 10);
        System.out.println("Фигура номер один: ");
        Figure.tipe(len);
        System.out.println("Фигура номер два: ");
        Figure.tipe(ben);
        System.out.println("Фигура номер три: ");
        Figure.tipe(ran);


    }
}