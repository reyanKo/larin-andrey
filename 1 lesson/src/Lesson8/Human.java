package Lesson8;

public class Human {
    private String name;
    private String sname;
    private int age;
    private boolean isMale;

    public String getName() {
        return name;
    }

    public String getSname() {
        return sname;
    }

    public int getAge() {
        return age;
    }

    public boolean isMale() {
        return isMale;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Human() {
        name = "lol";
        sname = "slol";
        age = 0;
        isMale = true;
    }

    public Human(String name, String sname, int age, boolean male) {
        this.name = name;
        this.sname = sname;
        this.age = age;
        this.isMale = male;
    }

    public void prnt() {
        System.out.print(name + " " + sname + " " + age + " ");
        if (isMale) {
            System.out.println("Мужчина");
        } else {
            System.out.println("Женщина");
        }
    }
}
