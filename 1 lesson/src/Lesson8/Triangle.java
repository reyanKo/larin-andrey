package Lesson8;

public class Triangle {
    private int a;
    private int b;
    private int c;

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    public int getC() {
        return c;
    }

    public void setC(int c) {
        this.c = c;
    }

    {
        a = 1;
        b = 1;
        c = 1;
    }

    public Triangle(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public static void print(Triangle stat) {
        System.out.println("Треугольник со сторонами : " + stat.getA() + " " + stat.getB() + " " + stat.getC());
    }

    public static int perim(Triangle stat) {
        int tmp = 0;

        tmp = stat.getA() + stat.getB() + stat.getC();

        return tmp;
    }

    public static double geron(Triangle stat) {
        double ger = 0;
        double p = 0.5 * (stat.getA() + stat.getB() + stat.getC());
        ger = Math.sqrt(p * (p - stat.getA()) * (p - stat.getB()) * (p - stat.getC()));

        return ger;
    }

    public static void check(Triangle stat) {
        if (stat.getA() == stat.getB()) {
            if (stat.getB() != stat.getC()) {
                System.out.println("Ваш треугольник равнобедренный");
            } else if (stat.getB() == stat.getC()) {
                System.out.println("Ваш треугольник равносторонний");
            }
        } else {
            System.out.println("Ваш треугольник очень странный :D ");
        }
    }
}
