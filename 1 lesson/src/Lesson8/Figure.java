package Lesson8;

public class Figure {
    int length;
    int width;
    int height;


    public Figure(int length) {
        this.length = length;
        this.height = 0;
        this.width = 0;
    }

    public Figure(int length, int height) {
        this.length = length;
        this.height = height;
        this.width = 0;
    }

    public Figure(int length, int height, int width) {
        this.length = length;
        this.width = width;
        this.height = height;
    }

    public static void tipe(Figure first) {
        if (first.length > 0 && first.width==0 && first.height==0) {
            System.out.println("прямая");
        } else if ( first.length>0 && first.height>0 && first.width==0){
            System.out.println("прямоугольник");
        } else  if (first.length>0 && first.width>0 && first.height>0){
            System.out.println("паралелепипед");
        }
    }
}