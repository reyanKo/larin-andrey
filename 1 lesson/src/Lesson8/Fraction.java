package Lesson8;

public class Fraction {
    private int x1;
    private int x2;

    public int getX1() {
        return x1;
    }

    public void setX1(int x1) {
        this.x1 = x1;
    }

    public int getX2() {
        return x2;
    }

    public void setX2(int x2) {
        this.x2 = x2;
    }

    public Fraction() {
        x1 = 0;
        x2 = 0;
    }

    public Fraction(int x1, int x2) {
        this.x1 = x1;
        this.x2 = x2;
    }

    public void print() {
        System.out.println(getX1());
        System.out.println("-");
        System.out.println(getX2());
    }
}

class FractionLogic {
    public static Fraction multiply(Fraction first, Fraction second) {
        Fraction res = new Fraction();

        res.setX1(first.getX1() * second.getX1());
        res.setX2(first.getX2() * second.getX2());

        return res;
    }

    public static Fraction division(Fraction first, Fraction secong) {
        Fraction res = new Fraction();

        res.setX1(first.getX1() * secong.getX2());
        res.setX2(first.getX2() * secong.getX1());

        return res;
    }

    public static Fraction plus(Fraction first, Fraction second) {
        Fraction res = new Fraction();

        res.setX1(first.getX1() * second.getX2() + second.getX1() * first.getX2());
        res.setX2(first.getX2() * second.getX2());

        return res;
    }

    public static Fraction minus(Fraction first, Fraction second) {
        Fraction res = new Fraction();

        res.setX1(first.getX1() * second.getX2() - second.getX1() * first.getX2());
        res.setX2(first.getX2() * second.getX2());

        return res;
    }
}