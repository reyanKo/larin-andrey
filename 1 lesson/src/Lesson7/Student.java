package Lesson7;


public class Student {
    String sname;
    String inic;
    int nomg;
    int[] uspev = new int[5];

    public void Student(String a, String b, int c, int ...array) {
        this.sname = a;
        this.inic = b;
        this.nomg = c;
        this.uspev = array;
    }

    public void print() {
        System.out.println("Фамилия и инициалы: " + this.sname + " " + this.inic);
        System.out.println("Номер группы: " + this.nomg);
        System.out.println("Ваши оценки: ");
        for (int i = 0; i < uspev.length; i++) {
            System.out.print(" " + this.uspev[i]+" ");
        }
        System.out.println();
    }

    void print45(){
        boolean tmp = false;
        for (int i = 0; i < uspev.length; i++) {
            if (uspev[i]>4){
                tmp = true;
            }
        }
        if (tmp==true){
            print();
        }
    }

    public int sredbal(){
        int bam = 0;
        int vam = 0;
        int sred = 0;
        for (int i = 0; i <uspev.length; i++) {
            vam+=uspev[i];
            bam++;            
        }
        sred = vam/bam;
        return sred;
    }
}
