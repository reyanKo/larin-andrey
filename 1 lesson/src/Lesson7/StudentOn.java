package Lesson7;

import java.util.Scanner;

public class StudentOn {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Student[] student = new Student[10];
        for (int i = 0; i < student.length; i++) {
            student[i] = new Student();
            System.out.println("Введите Имя и Инициалы: ");
            student[i].sname = scanner.nextLine();
            student[i].inic = scanner.nextLine();
            System.out.println("Введите группу: ");
            student[i].nomg = scanner.nextInt();
            System.out.println("Введите оценки ученика (5 штук) : ");
            for (int j = 0; j < student[i].uspev.length; j++) {
                student[i].uspev[j] = scanner.nextInt();
            }
        }
        for (int i = 0; i < student.length; i++) {
            student[i].print();
        }
        for (int i = 0; i < student.length-1; i++) {
            if (student[i].sredbal() > student[i + 1].sredbal()) {
                Student bam = new Student();
                bam = student[i];
                student[i] = student[i + 1];
                student[i + 1] = bam;
            }
        }
        for (int i = 0; i < student.length; i++) {
            student[i].print();
        }

        System.out.println("Студенты у кого оценки выше 4: ");
        for (int i = 0; i < student.length; i++) {
            student[i].print45();
        }

    }
}
