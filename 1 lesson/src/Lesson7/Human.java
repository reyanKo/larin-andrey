package Lesson7;

import java.util.Scanner;

public class Human {
    String name;
    String sname;
    int age;
    String male;

    {
        name = "Путешественник";
        sname = "Номер 1";
        age = 0;
        male = "man";
    }

    Human(String name, String sname, int age, String male) {
        this.name = name;
        this.sname = sname;
        this.age = age;
        this.male = male;
    }


    void print(){
        System.out.println("Твоё имя: "+name);
        System.out.println("Твоя фамилия: "+sname);
        System.out.println("Ваш возраст: "+age);
        System.out.println("Ваш пол: "+male);
    }
}
