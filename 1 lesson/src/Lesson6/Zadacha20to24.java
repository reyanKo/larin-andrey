package Lesson6;

import java.util.Random;
import java.util.Scanner;

public class Zadacha20to24 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Задайте нам любимый масив: ");
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        double[][] data = new double[a][b];
        randommas(data);
        System.out.println("Сумма всех эл-тов = " + summmas(data));
        print(data);
        System.out.println("Среднее арефметическое = " + sredar(data));
        System.out.println("Сумма диагонали и обратной диагонали массива: " + sumdiag(data) + " " + sumdiagob(data));
        System.out.println("Нормируем относительно макс по модулю: ");
        normmaspomax(data);
        print(data);
        randommas(data);
        System.out.println("Делим на 2 то что больше 10 по модулю: ");
        delnadva(data);
        print(data);
    }

    static void delnadva(double[][]array){
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if (Math.abs(array[i][j])>Math.abs(10)){
                    array[i][j]=array[i][j]/2;
                }
            }
        }
    }

    static void normmaspomax(double[][]array){
        double bam = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if (Math.abs(array[i][j])>Math.abs(bam)){
                    bam = Math.abs(array[i][j]);
                }
            }
        }
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = array[i][j]/bam;
            }
        }
    }

    static double sumdiagob(double[][] array) {
        double vam = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if (i + j == array.length - 1) {
                    vam += array[i][j];
                }
            }
        }
        return vam;
    }

    static double sumdiag(double[][] array) {
        double bam = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if (i == j) {
                    bam += array[i][j];
                }

            }
        }
        return bam;

    }

    static double sredar(double[][] array) {
        double bam = 0;
        double vam = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                vam++;
            }

        }
        bam = summmas(array) / vam;
        return bam;
    }

    static void print(double[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
    }

    static double summmas(double[][] array) {
        double bam = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                bam += array[i][j];
            }
        }
        return bam;
    }

    static void randommas(double[][] array) {
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = random.nextInt(15);
            }
        }
    }
}
