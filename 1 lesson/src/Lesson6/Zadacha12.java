package Lesson6;

import java.util.Random;
import java.util.Scanner;

public class Zadacha12 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Задайте размер масива: ");
        int a = scanner.nextInt();
        int[] data = new int[a];
        System.out.println("Задайте число K с которым мы работаем: ");
        int k = scanner.nextInt();
        Random random = new Random();
        for (int i = 0; i < data.length; i++) {
            data[i] = random.nextInt(15);
        }
        System.out.println("Вот наш масив: ");
        print(data);
        System.out.println();
        System.out.println("Мы сдивинули масив: ");
        sdvig(data);
        print(data);
        System.out.println();
        System.out.println("Минимальное число в нашем масиве: " + min(data));
        System.out.println("Максимальное число в нашем масиве: " + max(data));
        System.out.println("Все ли элименты отличаются? : " + razlich(data));
        System.out.println("Количество различных элементов в масиве: " + razel(data));
        System.out.println("Количество нулей в массиве: " + nuley(data));
        System.out.println("Кол-во элементов меньше K : " + mensh(data, k));
        print(data);

        System.out.println(razlich(new int[]{1,2,3}));

    }

    static int mensh(int[] data, int a) {
        int bam = 0;
        for (int i = 0; i < data.length; i++) {
            if (data[i] < a) {
                bam++;
            }
        }
        return bam;
    }

    static void sdvig(int[] data) {
        for (int i = 0; i < data.length - 1; i++) {
            int bam = data[i];
            data[i] = data[i + 1];
            data[i + 1] = bam;
        }
    }

    static int nuley(int[] data) {
        int bam = 0;
        for (int i = 0; i < data.length; i++) {
            if (data[i] == 0) {
                bam++;
            }
        }
        return bam;
    }

    static int razel(int[] data) {
        int bam = 0;
        for (int i = 0; i < data.length - 1; i++) {
            for (int j = 0; j < data.length; j++) {
                if (i != j & data[i] != data[j]) {
                    bam++;
                }break;
            }

        }
        return bam;
    }

    static boolean razlich(int[] data) {
        boolean bam = true;
        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < data.length; j++) {
                if (i !=j & data[i] == data[j]) {
                    bam = false;
                }
            }
        }
        return bam;
    }

    static int max(int[] data) {
        int bam = 0;
        for (int i = 0; i < data.length; i++) {
            if (data[i] > bam) {
                bam = data[i];
            }
        }
        return bam;
    }

    static int min(int[] data) {
        int bam = 100000000;
        for (int i = 0; i < data.length; i++) {
            if (data[i] < bam) {
                bam = data[i];
            }
        }
        return bam;
    }

    static void print(int[] data) {
        for (int i = 0; i < data.length; i++) {
            System.out.print(data[i] + " ");
        }
    }
}
