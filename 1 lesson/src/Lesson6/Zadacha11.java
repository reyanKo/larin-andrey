package Lesson6;

import java.util.Random;

public class Zadacha11 {
    public static void main(String[] args) {
        int[] data = new int[10];
        Random random = new Random();
        for (int i = 0; i < data.length; i++) {
            data[i] = random.nextInt(10);
        }
        for (int i = 0; i < data.length; i++) {
            System.out.print(data[i] + " Это наш масив ");
        }
        System.out.println();
        for (int i = 0; i < data.length/2; i++) {
            int bam = data[i];
            data[i]= data[data.length-1-i];
            data[data.length-1-i] = bam;
        }
        for (int i = 0; i < data.length; i++) {
            System.out.print(data[i] + " Это мы перевернули масив ");
        }

    }
}
