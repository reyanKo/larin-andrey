package Lesson6;

import java.util.Random;

public class Zadacha10 {
    public static void main(String[] args) {
        int[] data = new int[10];
        Random random = new Random();
        for (int i = 0; i < data.length; i++) {
            data[i] = random.nextInt(10);
        }
        int bam = 0;
        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < data.length - 1; j++) {
                if (data[j] < data[j + 1]) {
                    bam = data[j];
                    data[j] = data[j + 1];
                    data[j + 1] = bam;
                }
            }
        }

        for (int i = 0; i < data.length; i++) {
            System.out.print(data[i] + " ");

        }
        System.out.println();
        System.out.println("Три наибольших числа: " + " " + data[0] + " " + data[1] + " " + data[2]);
    }
}
