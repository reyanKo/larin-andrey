package Lesson10;

public class OrcShaman extends Orc {

    {
        this.dmg = 5000;
        this.def = 0;
        this.hp = 0;
        this.dmgtipe = "magical";
    }

    @Override
    public void Attack() {
        System.out.println(this.dmgtipe + " " + this.dmg);

    }
}
