package Lesson10;

public class Chinchilla extends Rodent {
    public void run() {
        System.out.println("Шиншила бежит со скоростью " + super.speed);
    }

    public void jump() {
        System.out.println("Прыгаем");
    }

    public void eat() {
        System.out.println("Кушаем");
    }
}
