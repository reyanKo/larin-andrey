package Lesson10;

public class OrcPeon extends Orc {

    {
        this.dmg = 100;
        this.def = 0;
        this.hp = 0;
        this.dmgtipe = "crushing";
    }


    @Override
    public void Attack() {
        System.out.println(this.dmgtipe + " " + this.dmg);

    }
}
