package Lesson10;

public class Hamster extends Rodent {
    public void run() {
        System.out.println("Хомяк бежит со скоростью " + super.speed);
    }

    public void jump() {
        System.out.println("Прыгаем");
    }

    public void eat() {
        System.out.println("Кушаем");
    }

}
