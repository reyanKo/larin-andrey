package Lesson10;

public abstract class Orc {
    int dmg;
    int def;
    int hp;
    String dmgtipe;

    {
        this.dmg = 0;
        this.def = 0;
        this.hp = 0;
        this.dmgtipe = "normal";
    }

    public abstract void Attack();
}
