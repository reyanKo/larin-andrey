package Lesson10;

public abstract class Rodent {
    String male;
    int weight;
    String color;
    int speed;


    public abstract void run();
    public abstract void jump();
    public abstract void eat();
}
