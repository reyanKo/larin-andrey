package Lesson10;

public class OrcGrunt extends Orc {

    {
        this.dmg = 30;
        this.def = 0;
        this.hp = 0;
        this.dmgtipe = "hacking";
    }

    @Override
    public void Attack() {
        System.out.println(this.dmgtipe + " " + this.dmg);

    }
}
