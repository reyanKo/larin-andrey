package Lesson9;

public class Buy extends Product {

    private int allpr;
    private double allw;

    public int getAll() {
        return allpr;
    }

    public void setAll(int allpr) {
        this.allpr = allpr;
    }

    public double getAprice() {
        return allw;
    }

    public void setAprice(double allw) {
        this.allw = allw;
    }



    public int allpr(Product[] products) {
        int tmp = 0;
        int bam = 0;
        for (int i = 0; i < products.length; i++) {
            tmp++;
            bam += products[i].getPrice();
        }
        return allpr = bam * tmp;
    }

    public double allw(Product[] products) {
        double tmp = 0;
        for (int i = 0; i < products.length; i++) {
            tmp += products[i].getWeight();
        }
        return tmp;
    }
}
