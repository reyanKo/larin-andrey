package Lesson4;

import java.util.Random;

public class Sortir {
    public static void main(String[] args) {
        double[] mass = new double[10];
        Random random = new Random();

        for (int i = 0; i < mass.length; i++) {
            mass[i] = random.nextInt(10);

        }

        double bob = 0;

        for (int j = mass.length - 1; j > 0; j--) {
            for (int k = 0; k < j; k++) {
                if (mass[k] > mass[k + 1]) {
                    bob = mass[k];
                    mass[k] = mass[k + 1];
                    mass[k + 1] = bob;
                }

            }

        }
        for (int i = 0; i < mass.length; i++) {
            System.out.println(mass[i]);

        }

    }
}

