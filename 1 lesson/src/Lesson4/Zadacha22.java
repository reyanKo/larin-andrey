package Lesson4;

import java.util.Random;

public class Zadacha22 {
    public static void main(String[] args) {
        double[][] mass = new double[10][10];
        Random random = new Random();

        for (int i = 0; i <mass.length ; i++) {
            for (int j = 0; j <mass[i].length ; j++) {
                mass[i][j] = random.nextInt(10);

            }

        }

        double summa = 0;
        for (int i = 0; i <mass.length ; i++) {
            for (int j = 0; j <mass[i].length ; j++) {
                if (i==j) {
                    summa +=mass[i][j];
                }

            }

        }
        System.out.println(summa);

    }
}
