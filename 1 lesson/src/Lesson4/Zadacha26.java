package Lesson4;

import java.sql.SQLOutput;
import java.util.Random;
import java.util.Scanner;

public class Zadacha26 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите размеры масива: ");
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        double[][] data = new double[a][b];
        System.out.println("Введите разброс рандома: ");
        int c = scanner.nextInt();
        randommas(data, c);
        printmas(data);
        System.out.println("Максимум масива = " + maximmas(data));
        System.out.println("Нормированный масив: ");
        normmas(data);
        printmas(data);


    }
    static void normmas(double[][] array){
        double a = 0;
        for (int i = 0; i <array.length ; i++) {
            for (int j = 0; j <array.length ; j++) {
                a = array[i][j]/maximmas(array);
                array[i][j] = a;

            }

        }
    }
    static void printmas(double[][] array){
        for (int i = 0; i <array.length ; i++) {
            for (int j = 0; j <array.length ; j++) {
                System.out.print(array[i][j] + " ");

            }
            System.out.println();

        }

    }

    static double maximmas(double[][] array) {
        double max = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                if (
                        array[i][j] > max) {
                    max = array[i][j];
                }

            }

        }return max;
    }

    static void randommas(double[][] array, int a) {
        Random random = new Random();

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                array[i][j] = random.nextInt(a);

            }

        }
    }
}
