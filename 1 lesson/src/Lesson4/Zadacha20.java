package Lesson4;

import java.util.Random;

public class Zadacha20 {
    public static void main(String[] args) {
        double[][] mass = new double[10][10];
        Random random = new Random();

        for (int i = 0; i <mass.length ; i++) {
            for (int j = 0; j <mass[i].length ; j++) {
                mass[i][j] = random.nextInt(10);

            }


        }

        double summ = 0;
        for (int i = 0; i <mass.length ; i++) {
            for (int j = 0; j <mass[i].length ; j++) {
                summ += mass[i][j];

            }

        }
        System.out.println(summ);
    }
}
