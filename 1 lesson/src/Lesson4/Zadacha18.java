package Lesson4;

import java.util.Arrays;
import java.util.Random;

public class Zadacha18 {
    public static void main(String[] args) {
        int a = 10;
        double[][] matrix = new double[a][a];
        Random random = new Random();

        for (int i = 0; i < matrix.length ; i++) {
            for (int j = 0; j <matrix[i].length ; j++) {
                matrix[i][j] = random.nextInt(10) -2;


            }

        }

        double minmas = Double.MAX_VALUE;
        for (int i = 0; i <matrix.length ; i++) {
            for (int j = 0; j <matrix[i].length ; j++) {
                if (matrix[i][j]<minmas){
                    minmas = matrix[i][j];
                }

            }

        }
        System.out.println(minmas);
    }
}
