package Lesson4;

import java.util.Random;

public class Zadacha8i9i10 {
    public static void main(String[] args) {
        double[] proiz = new double[10];
        Random random = new Random();

        for (int i = 0; i < proiz.length; i++) {
            proiz[i] = random.nextInt(10) -10 ;

        }

        double proizchet = 1;
        double proiznechet = 1;
        double ravnol = 0;

        for (int p = 0; p < proiz.length; p++) {
            if (p % 2 == 0) {
                proizchet *= proiz[p];
            } else if (p % 2 != 0) {
                proiznechet *= proiz[p];
            } else if (proiz[p] == 0) {
                ravnol ++;   // ВЕДЁТ СЧЁТЧИК
            }


        }
        System.out.println("Произв чет = " + proizchet);
        System.out.println("Произв не чет = " + proiznechet);
        System.out.println("Равно нулю = " + ravnol);
    }
}
