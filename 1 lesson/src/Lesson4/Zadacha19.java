package Lesson4;

import java.util.Random;

public class Zadacha19 {
    public static void main(String[] args) {
        double[][] massiv = new double[10][10];
        Random random = new Random();

        for (int i = 0; i <massiv.length ; i++) {
            for (int j = 0; j <massiv[i].length ; j++) {
                massiv[i][j] = random.nextInt(10);

            }

        }

        double maxel = Double.MAX_VALUE;
        for (int i = 0; i <massiv.length ; i++) {
            for (int j = 0; j <massiv[i].length ; j++) {
                if (massiv[i][j]<maxel) {
                    maxel = massiv[i][j];

            }

        }
        }
        System.out.println(maxel);

    }
}
