package Lesson4;

import java.util.Random;

public class Zadacha2 {
    public static void main(String[] args) {
        double[] max = new double[20];
        Random random = new Random();

        for (int i = 0; i < max.length; i++) {
            max[i] = random.nextInt(30) - 15;

        }
        double r = max[0];
        for (int p = 0; p < max.length; p++) {
            if (max[p] > r) {
                r = max[p];
            }
            System.out.println(r);

        }
        System.out.println("Максимум = " + r);

    }
}
