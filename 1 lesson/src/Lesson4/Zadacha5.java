package Lesson4;

import java.util.Random;

public class Zadacha5 {
    public static void main(String[] args) {
        double[] mass = new double[5];
        Random random = new Random();

        for (int i = 0; i <mass.length ; i++) {
            mass[i] = random.nextInt(15) -5;
            System.out.println(mass[i]);
        }

        double proizv = 1;
        for (int p = 0; p <mass.length ; p++) {
            proizv *= mass[p];

        }
        System.out.println("Произведение массива = " + proizv);
    }
}
